from threading import Thread
from time import sleep
import keyboard
from SocketThreads.FrontEndCommunicationManager import FrontendProcessor
from SocketThreads.IngestCommunicationManager import IngestCommunicationManager


class WebServer:

    def __init__(self):
        #TODO change addresses/ports
        self.frontend_processor = FrontendProcessor("localhost", 8193)
        self.ingest_receiver = IngestCommunicationManager('localhost', 5000, 1)
        self.__running = False

        self.frontend_comm = Thread(target=self.start_frontend_communication)
        self.ingest_listener = Thread(target=self.start_ingest_communication)
        self.input_listener = Thread(target=self.start_input_listener)
        self.start_ingest_communication()

    def start(self):
        if self.__running:
            return
        self.__running = True
        self.frontend_comm.start()
        self.ingest_listener.start()
        self.input_listener.start()

        self.ingest_receiver.start()

    def stop(self):
        if self.__running:
            return
        self.__running = False
        self.frontend_comm.join()
        self.ingest_listener.join()

    def start_frontend_communication(self):
        self.frontend_processor.start()
        while self.__running:
            self.frontend_processor.listen_for_connections(self)

    def start_ingest_communication(self):
        while self.__running:
            action = self.ingest_receiver.evaluate()
            if action is not None:
                print(action) #TODO handle action and send a response to the frontend

    def start_input_listener(self):
        while main.is_running():
            if keyboard.is_pressed('x') and keyboard.is_pressed('ctrl'):
                print("finishing")
                main.stop()
            sleep(0.1)

    def is_running(self):
        return self.__running


if __name__ == "__main__":
    main = WebServer()
    main.start()

