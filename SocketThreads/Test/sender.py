import socket # for socket
import sys

def sendMessage():
    clientsocket = socket.socket()
    host = "localhost"
    port = 8193
    clientsocket.connect((host, port))
    while True:
        name = input("Enter a message for server: ")
        clientsocket.send(name.encode())


if __name__ == "__main__":
    sendMessage()
