from flask import Flask
from flask import jsonify
from flask import request

app = Flask(__name__)


@app.route('/test', methods=['POST'])
def hello_world():
    data = request.json
    print(data)
    return str(data)


if __name__ == "__main__":
    app.run(debug=True)