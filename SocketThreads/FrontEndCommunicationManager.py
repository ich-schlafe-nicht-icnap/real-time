import socket
from threading import *


max_package_size_bytes = 1024


class Client(Thread):
    def __init__(self, socket, address, webserver):
        Thread.__init__(self)
        self.sock = socket
        self.addr = address
        self.webserver = webserver
        self.start()

    def run(self):
        while self.webserver.is_running():
            message = self.listen_for_messages()
            print('Client sent:', message)
            #TODO handle messages

    def listen_for_messages(self):
        return self.sock.recv(max_package_size_bytes).decode()

    def send(self, json_message):
        self.sock.send(json_message.encode())


class FrontendProcessor:

    def __init__(self, address, port):
        self.address = address
        self.port = port
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clients = []

    def start(self):
        self.serversocket.bind((self.address, self.port))
        self.serversocket.listen(5)
        print('Webclient server socket started and listening at port {addr}:{port}'.format(addr=self.address, port=self.port))

    def listen_for_connections(self, webserver):
        clientsocket, address = self.serversocket.accept()
        self.clients.append(Client(clientsocket, address, webserver))


    def send_all(self, json_message):
        for client in self.clients:
            client.send(json_message)
