import json


def dict_to_json(dict_convertible: dict):
    return json.dumps(dict_convertible)


def json_to_dict(json_convertible: str):
    return json.loads(json_convertible)
