import json
from flask import Flask
from flask import request
import datetime

Runstr = "Run"
Errorstr = "Error"
AutoStr = "Auto"


class IngestStatus:
    def __init__(self, status:bool, timestamp:datetime.datetime, evaluated:bool=False):
        self.status = status
        self.timestamp = timestamp
        self.evaluated = evaluated


class IngestCommunicationManager:

    def __init__(self, address, port, max_not_running_min=15):
        self.address = address
        self.port = port
        self.latest_datetime = None
        self.latest_event_count = -1
        now = datetime.datetime.now()
        self.latest_status = {Runstr: IngestStatus(False, now, True),
                              Errorstr: IngestStatus(False, now, True),
                              AutoStr: IngestStatus(False, now, True)}
        self.max_not_running_min = max_not_running_min

    def start(self):
        print('Ingest listener started and listening at port {addr}:{port}'.format(addr=self.address, port=self.port))
        app = Flask(__name__)

        @app.route('/datetime', methods=['POST'])
        def new_date_time():
            data = request.json
            self.process_new_date_time(data)
            return data

        @app.route('/eventcounter', methods=['PUT'])
        def update_event_counter():
            data = request.json
            self.process_update_event_counter(data)
            return data

        @app.route('/status', methods=['PUT'])
        def update_status():
            data = request.json
            self.process_update_status(data)
            return data

        app.run(debug=False, host=self.address, port=self.port)

    def process_new_date_time(self, message):
        self.latest_datetime = datetime.datetime.strptime(message['datetime'], '%Y-%m-%dT%H:%M:%S.%f')

    def process_update_event_counter(self, message):
        self.latest_event_count = message['count']

    def process_update_status(self, message):
        self.latest_status[message['ty']] = IngestStatus(message['status'], self.latest_datetime)

    def elapsed_seconds(self, timestamp):
        if timestamp is None:
            return -1
        now = datetime.datetime.now()
        diff = now - timestamp
        return int(diff.seconds)

    def evaluate(self):
        error_status = self.latest_status[Errorstr]
        if not error_status.evaluated and error_status.status:
            error_status.evaluated = True
            return "error" #TODO return an action
        run_status = self.latest_status[Runstr]
        if not run_status.evaluated and not run_status.status:
            if self.elapsed_seconds(run_status.timestamp) >= self.max_not_running_min * 60:
                run_status.evaluated = True
                return "run" #TODO return an action
        auto_status = self.latest_status[AutoStr]
        if not auto_status.evaluated and not auto_status.status:
            #TODO, what needs to happen here?
            auto_status.evaluated = True
            return "auto"
        return None

